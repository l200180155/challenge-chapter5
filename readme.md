# RESTful API dengan Express, Sequelize, dan ejs

Project ini menggunakan NodeJS dan module Express, Sequelize, ImageKit, dan EJS untuk View Engine

![alt text](./public/image/db-diagram.png)


## Install dependency

```bash
# menggunakan NPM
npm install

```

## run sequelize

```
sequelize db:create
sequelize db:migrate
sequelize db:seed:all
```

## Jalankan server

```
npm run dev
```
## Database Diagram
![alt text](./public/image/db-diagram.png)